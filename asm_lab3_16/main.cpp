#include <iostream>
#include <cstring>

using std::cout;
using std::endl;

int main()
{
    const char VOWELS[] = "aeiouyAEIOUY";
    const size_t VOWELS_SIZE = 12;
    
    char str[] = "eEeE The quick brown fox jumps over the lazy dog EeEe";
    size_t strSize = strlen(str);
    
    cout << "Before:" << endl;
    cout << str << endl;
    cout << endl;

    __asm
    {
            // esi -- address of str
            // ecx -- length of str

            // edi -- address of VOWELS


            lea esi, str        // esi <- &str
            mov ecx, strSize    // ecx <- strSize

            lea edi, VOWELS     // edi <- &VOWELS

        next_element:
            push edi // сохраним адрес VOWELS

            mov al, [esi]   // al <- str[esi]
            mov bl, [edi]   // bl <- VOWELS[edi]

            cmp al, bl
            je vowel_found
            jmp vowel_not_found


        vowel_found:
            // сохраним адреса str, VOWELS
            // чтобы воспользоваться этими регистрами
            push esi
            push edi

            mov edi, esi
            inc esi
            
            push ecx
            rep movsb   // сдвигаем элементы
            pop ecx
            dec ecx     // ecx <- ecx - 1

            // вернём изначальные адреса str, VOWELS
            pop edi
            pop esi

            jmp check_again


        vowel_not_found:
            inc edi
            mov bl, [edi] // bl <- VOWELS[edi]

            cmp al, bl
            je vowel_found

            // вычисляем адрес последнего элемента строки VOWELS
            lea edx, VOWELS
            add edx, VOWELS_SIZE
            dec edx

            // проверяем, является ли str[esi] гласной буквой
            // пока не пробежим весь массив VOWELS
            cmp edi, edx
            jle vowel_not_found


        // переходим к следующему символу строки str
        inc esi
        dec ecx

        check_again:

        pop edi // вернём изначальные адрес VOWELS


        // продолжаем удалять гласные
        // пока не пробежим каждый символ строки str
        cmp ecx, 0
        jne next_element
    }

    cout << "After:" << endl;
    cout << str << endl;

    std::getchar();
    return 0;
}
