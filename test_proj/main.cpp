#include <iostream>
#include <cstring>

using std::cout;
using std::endl;

int main()
{
	char str1[] = "abcd";
	size_t str1_len = strlen(str1);

	char str2[] = "efgh";
	size_t str2_len = strlen(str2);

	__asm
	{
		lea esi, str1
		lea edi, str2
		mov ecx, str1_len
		rep movsb
	}

	cout << "str1: \"" << str1 << '"' << endl;
	cout << "str2: \"" << str2 << '"' << endl;

	std::getchar();
	return 0;
}