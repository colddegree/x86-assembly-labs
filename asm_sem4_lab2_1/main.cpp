#include <fstream> // ofstream
#include <limits> // infinity


// performance timer

#include <intrin.h> // __rdtsc

uint64_t rdtsc() {
    return __rdtsc();
}


#include <Windows.h> // Sleep

uint64_t getCpuFrequency() {
    uint64_t startTick, endTick;

    startTick = rdtsc();
    Sleep(1000); // 1 second delay
    endTick = rdtsc();

    return endTick - startTick;
}

// end of performance timer


typedef unsigned long long int big_int;
typedef double big_float;

big_float evaluatePi(big_int numberOfTerms) {

    big_float sum = 1;
    big_float signOfPreviousTerm = 1;

    big_int n = 1;

    for (big_int i = 0; i < numberOfTerms; ++i) {
        n += 2;
        sum += -signOfPreviousTerm / n;
        signOfPreviousTerm = -signOfPreviousTerm;
    }

    return sum * 4;
}

big_float evaluatePi_asm(big_int numberOfTerms) {

    big_float sum = 1;
    big_float signOfPreviousTerm = 1;

    big_int n = 1;

    const int ONE = 1;
    const int TWO = 2;
    const int FOUR = 4;

    big_int i = 0;

    __asm {
    nextTerm:
        // i = i + 1
        fild i
        fiadd ONE
        fisttp i


        // n = n + 2
        fild n
        fiadd TWO
        fist n



        fld signOfPreviousTerm  // ST(0) == signOfPreviousTerm
                                // ST(1) == n

        
        // signOfPreviousTerm = -signOfPreviousTerm
        fchs
        fst signOfPreviousTerm


        fdivr   // ST(0) == signOfPreviousTerm / n


        // sum = sum + signOfPreviousTerm / n
        fld sum
        fadd
        fstp sum



        fild i
        fild numberOfTerms  // ST(0) == numberOfTerms
                            // ST(1) == i


        // пока numberOfTerms не станет равен i, вычисляем следующее слагаемое (переходим к метке nextTerm)
        fcompp
        fstsw ax
        fwait
        sahf

        jne nextTerm


        // sum = sum * 4
        fld sum
        fild FOUR
        fmul
        fstp sum
    }

    return sum;
}


using std::endl;

int main() {

    std::ofstream fo("output.txt");

    big_float cppMinExecTime, asmMinExecTime;
    cppMinExecTime = asmMinExecTime = std::numeric_limits<big_float>::infinity();

    const big_int NUMBER_OF_TERMS = static_cast<big_int>(1e9 + 1e8 + 1e7);

    const size_t NUMBER_OF_ITERATIONS = 15;


    for (size_t i = 0; i < NUMBER_OF_ITERATIONS; ++i) {

        uint64_t startTick, endTick;
        big_float pi;


        // вычисление числа Пи с использованием C++
        startTick = rdtsc();
        pi = evaluatePi(NUMBER_OF_TERMS);
        endTick = rdtsc();

        auto ticks = endTick - startTick;
        auto time = static_cast<big_float>(ticks) / getCpuFrequency();

        cppMinExecTime = min(time, cppMinExecTime);



        // вычисление числа Пи с использованием ассемблерных вставок
        startTick = rdtsc();
        pi = evaluatePi_asm(NUMBER_OF_TERMS);
        endTick = rdtsc();

        ticks = endTick - startTick;
        time = static_cast<big_float>(ticks) / getCpuFrequency();

        asmMinExecTime = min(time, asmMinExecTime);
    }
    
    fo << "Минимальное время вычисления числа Пи с использованием" << endl;
    fo << "\tC++:                  " << cppMinExecTime << " сек" << endl;
    fo << "\tАссемблерных вставок: " << asmMinExecTime << " сек" << endl;


    return 0;
}
