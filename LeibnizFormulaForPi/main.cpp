#include <iostream>
#include <cmath> // abs
#include <limits> // infinity

#include <chrono>

template <typename T>
int sign(T value) {
    if (value > 0) return 1;
    if (value < 0) return -1;
    return 0;
}

typedef unsigned long long int big_int;
typedef long double big_float;

const big_float EPS = 1E-8;

using std::cout;
using std::endl;

int main() {

    big_float sum = 1;
    big_float prevSummand = std::numeric_limits<big_float>::infinity();

    auto startTime = std::chrono::steady_clock::now();

    for (big_int n = 3; std::abs(prevSummand) > EPS; n += 2) {
        big_float summand = -sign(prevSummand);
        summand /= n;
        sum += summand;
        prevSummand = summand;
    }

    auto endTime = std::chrono::steady_clock::now();

    big_float pi = sum * 4;

    cout.precision(9);
    cout << pi << endl;
    cout << std::chrono::duration< double, std::ratio<1> >(endTime - startTime).count() << " secs" << endl;

    return 0;
}