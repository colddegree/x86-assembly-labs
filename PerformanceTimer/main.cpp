// https://helloacm.com/the-rdtsc-performance-timer-written-in-c/
#include <iostream>
#include <cstdlib>
#include <stdint.h>

#include <unistd.h> // sleep()

//  Windows
#ifdef _WIN32

#include <x86intrin.h>
uint64_t rdtsc(){
    return __rdtsc();
}

//  Linux/GCC
#else

uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

#endif

uint64_t getCpuFrequency() {
    uint64_t ts0, ts1;
    ts0 = rdtsc();
    sleep(1);
    ts1 = rdtsc();
    return ts1 - ts0;
}

using namespace std;

int main(int argc, char* argv[]) {
    uint64_t tick = rdtsc();  // tick before
    for (int i = 1; i < argc; ++ i) {
        system(argv[i]); // start the command
    }

    uint64_t endTime = rdtsc() - tick;

    // difference
    cout << "[rdtsc]: " << endTime << " cycles" << endl;
    cout << "[rdtsc]: " << static_cast<long double>(endTime) / getCpuFrequency()
         << " seconds" << endl;

    return 0;
}